//
//  Place.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class Place : Object, Mappable {
    dynamic var name : String!
    dynamic var latitude : Double = 0.0
    dynamic var longitude : Double = 0.0
    dynamic var distance : Double = 0.0
    
    dynamic var city : String?
    dynamic var country : String?
    dynamic var rating : Float = 0.0
    dynamic var url : String?
//    var canonicalUrl : String?
    
    required convenience init?(map: Map) {
        self.init()
    }
        
    func mapping(map: Map) {
        name <- map["venue.name"]
        latitude <- map["venue.location.lat"]
        longitude <- map["venue.location.lng"]
        distance <- map["venue.location.distance"]
//        canonicalUrl <- map["tips.0.canonicalUrl"]
        city <- map["venue.location.city"]
        country <- map["venue.location.country"]
        country <- map["rating"]
        url <- map["url"]
    }
}
