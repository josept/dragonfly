//
//  FireflyCache.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import Foundation
import RealmSwift



class FireFlyCache {
    
    //Load cache... only one element saved
    class func loadCache() -> PlacesResponse? {
        let realm = try! Realm()
        
        let placesCached = realm.objects(PlacesResponse.self).first
        
        
        return placesCached
    }
    
    //Save response from backend in cache and call delegate
    class func writeCache (serviceCallback : APICallbacks, result : Dictionary <String,Any>, delegate : FireflyAPIDelegate) {
        
        FireFlyCache.deleteCache()
        
        if let placesResp   = result["result"] {
            let places      = placesResp as! PlacesResponse
            
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(places)
            }
            
            let placesCached = realm.objects(PlacesResponse.self).first
            print(placesCached!)
            
            delegate.fetchEnd(serviceCallback: serviceCallback, result: result)
        }
    }
    
    //Override cache
    class func deleteCache () {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
}
