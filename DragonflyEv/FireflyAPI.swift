//
//  FireflyAPI.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper

class FireflyAPI : NSObject {

    
    
    //MARK: Functions to call services
    
    class func exploreLocations ( latitude : Double, longitude : Double, distance : Float, delegate : FireflyAPIDelegate) {
        
        let urlService      = "https://api.foursquare.com/v2/venues/explore?"
        let clientID        = "client_id=AV5IJIAPDKHAAZRSEPQRKEXTJAS4ZAXELQMAG02RNUALQOXZ"
        let clientSecret    = "&client_secret=0LXVD3WGCPAARJGZ4VIOD1JQSJV2MH3BRYB1USSEDSXX4KI3"
        let versionParam    = "&v=20170801"
        let llParameter     = "&ll=\(latitude),\(longitude)"
        let radiusParameter = "&radius=\(distance)"
        let limitParameter  = "&limit=10"
        
        let completeUrl = urlService + clientID + clientSecret + versionParam + llParameter + radiusParameter + limitParameter
        
//        Alamofire.request(completeUrl).responseJSON { response in
//            if let result = response.result.value {
//                let JSON = result as! Dictionary<String, Any>
//                let response = JSON["response"] as! Dictionary<String, Any>
//                let groups = response["groups"] as! Array<Dictionary<String,Any>>
//                let items = groups[0]["items"]!
//                
//                //print(items)
//                print(result)
//            }
//        }
        
        Alamofire.request(completeUrl).responseObject { (response: DataResponse<PlacesResponse>) in
            if let result = response.result.value {
                print(result)
                print(response.response?.statusCode ?? 0)
                
                let statusCode = response.response?.statusCode ?? 0
                
                switch statusCode {
                    case 200:
//                        for place in result.places! {
//                            print(place.name!)
//                            print(place.distance ?? 0)
//                            print(place.latitude!)
//                            print(place.longitude!)
//                        }
                        
                        //delegate.fetchEnd(serviceCallback: APICallbacks.explore, result: ["result":result])
                        FireFlyCache.writeCache(serviceCallback: APICallbacks.explore, result: ["result":result], delegate: delegate)
                        
                        break;
                    
                    case 400:
                        delegate.fetchError(serviceCallback: APICallbacks.explore, error: APIErrors.invalidCredentials)
                        break;
                    case 500:
                        delegate.fetchError(serviceCallback: APICallbacks.explore, error: APIErrors.serverError)
                        break;
                    
                    case 404:
                        delegate.fetchError(serviceCallback: APICallbacks.explore, error: APIErrors.notFound)
                        break;
                        
                    default:
                        delegate.fetchError(serviceCallback: APICallbacks.explore, error: APIErrors.unknownError)
                        break;
                }
                
                
            }
        }
        
    }
    
    
}
