//
//  PlacesResponse.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift
import Realm
import ObjectMapper_Realm

class PlacesResponse: Object, Mappable {
    dynamic var requestId : String!
    var places: List<Place> = List<Place>()
    
    required convenience init?(map: Map){
        self.init()
    }
    
    func mapping(map: Map) {
        requestId <- map["meta.requestId"]
        places <- (map["response.groups.0.items"],ListTransform<Place>())
    }
}
