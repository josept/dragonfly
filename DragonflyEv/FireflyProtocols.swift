//
//  FireflyProtocols.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import Foundation

/*
 * In case we have multiple services, we can different among them with a callback enum
 */
enum APICallbacks {
    case explore
}

enum APIErrors : Error {
    case unknownError
    case invalidCredentials
    case notFound
    case serverError
}

protocol FireflyAPIDelegate {
    
    ///To catch fetch's end
    func fetchEnd( serviceCallback : APICallbacks, result : Dictionary <String,Any>)
    
    ///To catch fetch's errors
    func fetchError (serviceCallback : APICallbacks, error : APIErrors)
}
