//
//  ViewController.swift
//  DragonflyEv
//
//  Created by Carlos Fuentes on 14/10/17.
//  Copyright © 2017 Carlos Fuentes. All rights reserved.
//

import UIKit
import CoreLocation
import GoogleMaps
import RealmSwift

class ViewController: UIViewController, CLLocationManagerDelegate, UITableViewDelegate, UITableViewDataSource, FireflyAPIDelegate {

    var locationManager = CLLocationManager()
    var userLocation = CLLocationCoordinate2D()
    var mapView : GMSMapView?
    var firstLoad : Bool = false
    
    var placesCached : List<Place> = List<Place>()
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var noCacheView: UIView!
    @IBOutlet weak var placesTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        checkConnection()
        
    }

    func checkConnection () {
        
        noCacheView.isHidden        = true
        placesTableView.isHidden    = true
        firstLoad                   = false
        
        locationManager.stopUpdatingLocation()
        locationManager = CLLocationManager()

        if mapView != nil {
            mapView?.removeFromSuperview()
            mapView = nil
        }
        
        
        if self.isInternetAvailable() {
            startUpdateLocation()
        }else {
            let places = FireFlyCache.loadCache()
            
            if places?.places != nil {
                placesCached = (places?.places)!
                
                placesTableView.isHidden    = false
                placesTableView.delegate    = self
                placesTableView.dataSource  = self
                
                placesTableView.reloadData()
                
            }else {
                noCacheView.isHidden = false
            }
        }
    }

    //MARK: Location delegates
    func startUpdateLocation () {
        //Authorization
        self.locationManager.delegate        = self
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            locationManager.requestAlwaysAuthorization()
        }else{
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationValue : CLLocationCoordinate2D = (locations.last?.coordinate)!
        
        if !firstLoad {
            firstLoad = true
            userLocation = locationValue
            FireflyAPI.exploreLocations(latitude: locationValue.latitude, longitude: locationValue.longitude, distance: 1000, delegate: self)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            manager.startUpdatingLocation()
            break
        case .denied:
            //handle denied
            break
        case .notDetermined:
            manager.requestAlwaysAuthorization()
            break
        default:
            break
        }
    }


    //MARK: Firefly API Delegates
    
    func fetchEnd(serviceCallback: APICallbacks, result: Dictionary<String, Any>) {
        if let placesResp   = result["result"] {
            let places      = placesResp as! PlacesResponse
            showGoogleMap(places: places)
        }
    }
    
    func fetchError(serviceCallback: APICallbacks, error: APIErrors) {
        print("Error!!")
        
    }
    
    //MARK: Show map
    func showGoogleMap( places : PlacesResponse ) {
        let camera                  = GMSCameraPosition.camera(withLatitude: userLocation.latitude, longitude: userLocation.longitude, zoom: 16.0)
        mapView                     = GMSMapView(frame: self.view.frame)
        mapView?.isMyLocationEnabled = true
        mapView?.animate(to: camera)
        
        self.view.addSubview(mapView!)
        
        addMarkers(places: places)
    }
    
    func addMarkers(places : PlacesResponse) {
        
        //Remove all markers
        mapView?.clear()
        
        //Add multiple markers
        for place in places.places {
            // Creates a marker in the center of the map.
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
            marker.title = place.name
            marker.iconView?.backgroundColor = UIColor.green
            marker.snippet = "\(place.city ?? ""), \(place.country ?? "")"
            marker.map = mapView
        }
    }
    
    //MARK: Table Delegates
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return placesCached.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell            = tableView.dequeueReusableCell(withIdentifier: "cell")
        
        let nameLbl         = cell?.viewWithTag(2) as! UILabel
        let locatedLbl      = cell?.viewWithTag(3) as! UILabel
        
        let currentPlace    = placesCached[indexPath.row]
        nameLbl.text        = currentPlace.name
        locatedLbl.text     = "\(currentPlace.city ?? ""), \(currentPlace.country ?? "")"
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //Uncomment for open urls in browser
        
//        let url = URL(string: placesCached[indexPath.row].url!)
//        
//        if UIApplication.shared.canOpenURL(url!) {
//            UIApplication.shared.open(url!, options: [:], completionHandler: nil)
//        }
    }
    
    //MARK: Refresh info
    @IBAction func refreshAction(_ sender: Any) {
        checkConnection()
    }
    
    
}

